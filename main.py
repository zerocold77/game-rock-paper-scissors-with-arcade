# import
import arcade
import arcade.gui
import random

# prints a random value from the list
list_choice = ["Rock", "Paper", "Scissors"]


class MyWindow(arcade.Window):
    def __init__(self):
        super().__init__(1100, 600, "Rock Paper Scissors")
        # Loading the background image
        self.background = arcade.load_texture("index.png")

        # --- Required for all code that uses UI element,
        # a UIManager to handle the UI.
        self.manager = arcade.gui.UIManager()
        self.manager.enable()
        # value
        self.copmputer = 0
        self.player = 0
        self.text_back = ""
        # style
        red_style = {
            "font_name": ("calibri", "arial"),
            "font_size": 15,
            "font_color": arcade.color.WHITE,
            "border_width": 2,
            "border_color": None,
            "bg_color": arcade.color.REDWOOD,

            # used if button is pressed
            "bg_color_pressed": arcade.color.WHITE,
            "border_color_pressed": arcade.color.RED,  # also used when hovered
            "font_color_pressed": arcade.color.RED,
        }

        # Set background color
        arcade.set_background_color(arcade.color.WHITE_SMOKE)

        # Create a vertical BoxGroup to align buttons
        self.v_box = arcade.gui.UIBoxLayout(space_between=10)
        # Create the buttons
        START = arcade.gui.UIFlatButton(text="START", width=200, style=red_style)
        self.v_box.add(START.with_space_around(bottom=20))
        Rock = arcade.gui.UIFlatButton(text="Rock", width=200)
        self.v_box.add(Rock.with_space_around(bottom=20))
        Paper = arcade.gui.UIFlatButton(text="Paper", width=200)
        self.v_box.add(Paper.with_space_around(bottom=20))
        Scissors = arcade.gui.UIFlatButton(text="Scissors", width=200)
        self.v_box.add(Scissors.with_space_around(bottom=20))
        # assign self.on_click_start as callback
        Rock.on_click = self.on_click_Rock
        Paper.on_click = self.on_click_Paper
        Scissors.on_click = self.on_click_Scissors
        START.on_click = self.on_click_START

        # Create a widget to hold the v_box widget, that will center the buttons
        self.manager.add(
            arcade.gui.UIAnchorWidget(
                align_x=200,
                align_y=20,
                child=self.v_box)
        )

    # function game
    def on_click_Rock(self,event):
        player_computer = (random.choice(list_choice))
        if player_computer == "Rock":
            self.text_back = f"Your:Rock & computer:{player_computer} => every equal"
        if player_computer == "Paper":
            self.text_back = f"Your:Rock & computer:{player_computer} => computer win"
            self.copmputer += 1
        if player_computer == "Scissors":
            self.text_back = f"Your:Rock & computer:{player_computer} => player win"
            self.player += 1

    def on_click_Paper(self, event):
        player_computer = (random.choice(list_choice))
        if player_computer == "Rock":
            self.text_back = f"Your:Paper & computer:{player_computer} => player win"
            self.player += 1
        if player_computer == "Paper":
            self.text_back = f"Your:Paper & computer:{player_computer} => every equal"
        if player_computer == "Scissors":
            self.text_back = f"Your:Paper & computer:{player_computer} => computer win"
            self.copmputer += 1

    def on_click_Scissors(self, event):
        player_computer = (random.choice(list_choice))
        if player_computer == "Rock":
            self.text_back = f"Your:Scissors & computer:{player_computer} => computer win"
            self.copmputer += 1
        if player_computer == "Paper":
            self.text_back = f"Your:Scissors & computer:{player_computer} =>  player win"
            self.player += 1
        if player_computer == "Scissors":
            self.text_back = f"Your:Scissors & computer:{player_computer} => every equal"

    def on_click_START(self, event):
        self.text_back = ""
        self.copmputer = 0
        self.player = 0

    def on_draw(self):
        self.clear()
        self.manager.draw()
        # Drawing the background image
        arcade.draw_texture_rectangle(200, 250, 700, 700, self.background)
        # show screen value
        output_computer = "Computer: " + str(self.copmputer)
        arcade.draw_text(output_computer, 600, 50, arcade.color.RED, 14)
        output_player = "Your: " + str(self.player)
        arcade.draw_text(output_player, 800, 50, arcade.color.RED, 14)
        arcade.draw_text(self.text_back, 570, 120, arcade.color.RED, 14)


window = MyWindow()
arcade.run()
